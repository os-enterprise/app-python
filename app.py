from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():
    return "Bienvenido al curso de Docker"

@app.route('/clase')
def hello():
    return 'Nos encontramos en la tercera clase'

@app.route('/image')
def image():
    return 'Estamos aprendiendo a crear imagenes'

app.run(host="0.0.0.0", port=8080)