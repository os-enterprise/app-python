FROM python:3.6.1-alpine
RUN pip install --upgrade pip && pip install flask && adduser -D flask
COPY . /
EXPOSE 5000
USER flask
ENTRYPOINT FLASK_APP=/app.py flask run --host=0.0.0.0